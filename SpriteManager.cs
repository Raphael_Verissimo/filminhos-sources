﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteManager : MonoBehaviour {

	[SerializeField]
	private GameObject IdleGO;
	private SpriteSheet idleSpriteSheet;

	[SerializeField]
	private GameObject WalkGO;
	private SpriteSheet walkSpriteSheet;

	[SerializeField]
	private GameObject TalkGO;
	private SpriteSheet talkSpriteSheet;

	[SerializeField]
	private GameObject SpecialGO;
	private SpriteSheet specialSpriteSheet;

	[SerializeField]
	private float interval;

	private float lastTime;

	private int currentIndex;
	private Image imageGO;
	private SpriteSheet currentSpriteSheet;
	private Vector3 invert;

	private ActionType currentAction;

	// Use this for initialization
	void Start ()
	{
		currentAction = ActionType.Idle;

		currentIndex = 0;
		lastTime = 0;
		imageGO = GetComponentInChildren<Image>(); 

		invert = new Vector3(-imageGO.transform.localScale.x, imageGO.transform.localScale.y, imageGO.transform.localScale.z);

		idleSpriteSheet = IdleGO.GetComponent<SpriteSheet>();
		walkSpriteSheet = WalkGO.GetComponent<SpriteSheet>();
		talkSpriteSheet = TalkGO.GetComponent<SpriteSheet>();
		specialSpriteSheet = SpecialGO.GetComponent<SpriteSheet>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateCurrentSprite ();
	}

	public void PlayIdle(){	currentAction = ActionType.Idle; }
	public void PlayTalk(){	currentAction = ActionType.Talk; }
	public void PlayWalk(){	currentAction = ActionType.Walk; }
	public void PlaySpecial(){	currentAction = ActionType.Special; }

	private void UpdateCurrentSprite()
	{
		switch (currentAction)
		{
			case ActionType.Idle:
				currentSpriteSheet = idleSpriteSheet;
			break;

			case ActionType.Walk:
				currentSpriteSheet = walkSpriteSheet;
			break;

			case ActionType.Talk:
				currentSpriteSheet = talkSpriteSheet;
			break;

			case ActionType.Special:
				currentSpriteSheet = specialSpriteSheet;
			break;
		}

		Debug.Log("Tempo : " + (Time.fixedTime - lastTime) + " Indice : " + currentIndex);
		if (Time.fixedTime - lastTime > interval) 
		{
			lastTime = Time.fixedTime;
			currentIndex = (currentIndex + 1) % currentSpriteSheet.sprites.Length;

			imageGO.transform.localScale = invert;
			imageGO.sprite = currentSpriteSheet.sprites[currentIndex];
		}
	}

	public enum ActionType
	{
		Idle, 
		Walk, 
		Talk, 
		Special
	}
}
